package com.tf.lite.playlagom;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.tf.lite.playlagom.classification.ClassifierActivity;
import com.tf.lite.playlagom.detection.DetectorActivity;

import org.tensorflow.lite.examples.classification.R;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    public static Context context;
    public static MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        // todo: app crash, getSupportActionBar().hide();

        context = MainActivity.this;
        mp = MediaPlayer.create(context, R.raw.person);
    }

    public void onClickClassifyImages(View view) {
        Intent intent = new Intent(MainActivity.this, ClassifierActivity.class);
        startActivity(intent);
    }

    public void onClickDetectObject(View view) {
        Intent intent = new Intent(MainActivity.this, DetectorActivity.class);
        startActivity(intent);
    }
}